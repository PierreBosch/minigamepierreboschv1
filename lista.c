#include <stdio.h>
#include <stdlib.h>
#include "lista.h"
#include "console.h"

struct lista {
    int x,y; // identificador do objeto
    char simbolo; // simbolo do elemento
    struct lista* prox; // ponteiro para o proximo elemento
};

Lista* lst_cria() {
    return NULL;
}

Lista* lst_insere(Lista* l, int x, int y, char simbolo) {

    if(lst_temElemento(l, x, y) == 1){
        return l;
    }

    Lista* novo   = (Lista*) malloc(sizeof(Lista));

    novo->x       = x;
    novo->y       = y;
    novo->simbolo = simbolo;


    novo->prox    = l;
    return novo;
}

void lst_imprime(Lista* l) {
    Lista* p;
    for(p = l; p != NULL; p = p->prox) {
        gotoxy(p->x, p->y);
        printf("%c", p->simbolo);
    }
}

int lst_temElemento(Lista* l, int x, int y) {

    Lista* a;

    for(a = l; a != NULL; a = a->prox) {
        if(a->x == x && a->y == y){
            return 1;
        }
    }
    return 0;
}

Lista* lst_retira(Lista* l, int x, int y) {

    Lista* ant   = NULL;
    Lista* atual = l;

    while(atual != NULL){
        if(atual->x == x && atual->y == y){
            if(ant == NULL){
                return atual->prox;
            } else{
                ant->prox = atual->prox;
            }
            free(atual);
        }
        ant   = atual;
        atual = atual->prox;

        return l;
    }
}


#include <stdio.h>
#include <stdlib.h>
#include <conio.h> // kbhit e getch
#include <unistd.h> // usleep
#include <time.h> // time
#include <windows.h> // Beep e SetConsoleTitle
#include "lista.h"
#include "console.h"


//x_ant, y_ant
int x_ant,y_ant;

// para a leitura de teclas
char tecla, tem_tecla;
// para o relogio
int tempo;
int pontos = 0;
time_t inicial, atual;
// personagem
struct personagem {
    int x,y;
    char avatar;
};
typedef struct personagem Personagem;

Personagem p;

// mapa
Lista* mapa1;
Lista* coletavel;

// cria mapa e objetos
void inicializaJogo() {
    // para o relogio
    tempo = 0;
    time(&inicial);

    // posicao inicial do personagem
    p.x = 1;
    p.y = 3;
    // avatar
    p.avatar = (char)2; // char

    // inicializa objetos no mapa
    mapa1 = lst_cria();
    coletavel = lst_cria();

    int i;

    //CONSTROI CIMA/BAIXO
    for(i = 0; i < 32; i++){
        mapa1 = lst_insere(mapa1, i, 0,(char)219);
        mapa1 = lst_insere(mapa1, i, 15,(char)219);
    }

    //CONSTROI LADOS
    for(i = 0; i < 16; i++){
        mapa1 = lst_insere(mapa1, 0, i,(char)219);
        mapa1 = lst_insere(mapa1, 31, i,(char)219);
    }

    //CONSTROI COLUNAS VERTICAIS
    for(i = 1; i <= 3; i++){
         mapa1 = lst_insere(mapa1, 7, i,(char)219);
         mapa1 = lst_insere(mapa1, 24,i,(char)219);

         mapa1 = lst_insere(mapa1, 7, i+11,(char)219);
         mapa1 = lst_insere(mapa1, 24,i+11,(char)219);
    }

    //CONSTROI Ls
    for(i = 3; i <= 6; i++){
         mapa1 = lst_insere(mapa1, 3, i,(char)219);
         mapa1 = lst_insere(mapa1, 28,i,(char)219);

         mapa1 = lst_insere(mapa1, 3, i+6,(char)219);
         mapa1 = lst_insere(mapa1, 28,i+6,(char)219);
    }

    //CONSTROI ponta dos Ls
    mapa1 = lst_insere(mapa1, 4,3,(char)219);
    mapa1 = lst_insere(mapa1, 27,3,(char)219);
    mapa1 = lst_insere(mapa1, 4,12,(char)219);
    mapa1 = lst_insere(mapa1, 27,12,(char)219);

    //CONSTROI 2 COLUNAS HORIZONTAIS 2 PX
    for(i = 6; i <= 7; i++){
         mapa1 = lst_insere(mapa1, i, 6,(char)219);
         mapa1 = lst_insere(mapa1, i, 9,(char)219);
    }
    //CONSTROI 2 COLUNAS HORIZONTAIS 2 PX
    for(i = 24; i <= 25; i++){
         mapa1 = lst_insere(mapa1, i, 6,(char)219);
         mapa1 = lst_insere(mapa1, i, 9,(char)219);
    }

    for(i = 10; i <= 21; i++){
         mapa1 = lst_insere(mapa1, i, 3,(char)219);
         mapa1 = lst_insere(mapa1, i, 12,(char)219);
    }

    for(i = 6; i <= 9; i++){
         mapa1 = lst_insere(mapa1, 10, i,(char)219);
         mapa1 = lst_insere(mapa1, 21, i,(char)219);
    }


    for(i = 11; i <= 18; i++){
         mapa1 = lst_insere(mapa1, i, 9,(char)219);
    }

    for(i = 13; i <= 20; i++){
         mapa1 = lst_insere(mapa1, i, 6,(char)219);
    }

    //insere 1 objeto
    //coletavel = lst_insere(coletavel, 2, 1,(char)3);
    //coletavel = lst_insere(coletavel, 15, 10,(char)4);
    //coletavel = lst_insere(coletavel, 18, 7,(char)5);
    //coletavel = lst_insere(coletavel, 25, 7,(char)6);
    //coletavel = lst_insere(coletavel, 15, 14,(char)219);
    //lst_imprime(coletavel);
    gerarItems(3);
}

void gerarItems(int qtd){
   int i,elementMapExists,elementItemExists;

    for(i = 1; i <= qtd;i++){
        do{
            int x = 1 + rand() % 32;
            int y = 1 + rand() % 16;

            elementMapExists = lst_temElemento(mapa1,x,y);
            elementItemExists = lst_temElemento(coletavel,x,y);

            if(elementMapExists == 0){
                if(elementItemExists == 0){
                    coletavel = lst_insere(coletavel,x, y,(char)219);
                }
            }
        }while(elementItemExists != 0 && elementMapExists != 0);

    }
    lst_imprime(coletavel);
}

// faz controle
void controle() {
    // atualiza relogio
    time(&atual);
    tempo = atual - inicial;

    // teclado
    if(tem_tecla == 1) {
        x_ant = p.x;
        y_ant = p.y;

        switch(tecla) {
            case TECLA_CIMA:
                if(lst_temElemento(mapa1, p.x, p.y-1) == 0){
                    if(lst_temElemento(coletavel, p.x, p.y-1) == 1){
                        lst_retira(coletavel, p.x, p.y-1);
                        gerarItems(1);
                        p.y--;
                        pontos++;
                    }else{
                        p.y--;
                    }
                }
                break;
            case TECLA_BAIXO:
                if(lst_temElemento(mapa1, p.x, p.y+1) == 0){
                    if(lst_temElemento(coletavel, p.x, p.y+1) == 1){
                        lst_retira(coletavel, p.x, p.y+1);
                        gerarItems(1);
                        p.y++;
                        pontos++;
                    }else{
                        p.y++;
                    }
                }
                break;
            case TECLA_ESQUERDA:
                if(lst_temElemento(mapa1, p.x-1, p.y) == 0){
                    if(lst_temElemento(coletavel, p.x-1, p.y) == 1){
                        lst_retira(coletavel, p.x-1, p.y);
                        gerarItems(1);
                        p.x--;
                        pontos++;
                    }else{
                        p.x--;
                    }
                }
                break;
            case TECLA_DIREITA:
                if(lst_temElemento(mapa1, p.x+1, p.y) == 0){
                    if(lst_temElemento(coletavel, p.x+1, p.y) == 1){
                        lst_retira(coletavel, p.x+1, p.y);
                        gerarItems(1);
                        p.x++;
                        pontos++;
                    }else{
                        p.x++;
                    }
                }
                break;
            case TECLA_ESC:
                exit(0);
                break;
        }
        tem_tecla=0;
    }
}

// rotina de leitura do teclado
void leTeclado() {
    if(kbhit()) { // se houve toque no teclado
        tecla = getch(); // armazena tecla pressionada
        tem_tecla = 1;
    }
}

// desenha mapa
void atualizaMapa() {

    // desenha personagem
    gotoxy(x_ant, y_ant);
    setBackgroundColor(COLOR_BLACK);
    printf("%c", 255);
    gotoxy(p.x,p.y);
    setColor(COLOR_WHITE);
    printf("%c", p.avatar);

    // relogio
    setColor(COLOR_WHITE);
    setBackgroundColor(COLOR_BLACK);
    gotoxy(12,17);
    printf("Tempo : %d    ", tempo);
    setBackgroundColor(COLOR_BLACK);
    setColor(COLOR_WHITE);
    gotoxy(12,18);
    printf("                 ");
    gotoxy(12,18);
    printf("Pontos: %d \n", pontos);

}

int main() {
    setBackgroundColor(COLOR_BLACK);
    system("cls");
    setWindow(32,20); // define tamanho da janela: linhas, colunas
    SetConsoleTitle("SE CORRER O BICHO PEGA - SE FICAR O BICHO COME"); // titulo da janela

    inicializaJogo();

    setColor(COLOR_YELLOW);
    lst_imprime(mapa1);

    while(1) {
        leTeclado();
        controle();
        atualizaMapa();
        usleep(1000); // aguarda um tempo

    }

     gotoxy(10,10);
     printf("a");
}
